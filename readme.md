- Create database: name: snap_page
- Change name file .env.example to .env and edit config database:
    DB_CONNECTION=mysql
    DB_HOST=yourhost
    DB_PORT=3306
    DB_DATABASE=snap_page
    DB_USERNAME=username
    DB_PASSWORD=password
- Open Teminal, cd to your project path run: 
    composer install
    php artisan key:generate
    php artisan migrate
    php artisan serve  
- Copy your path to browser to run project!
- All file library in /public
- Controller file in app/Http/Controllers