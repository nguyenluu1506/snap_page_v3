<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Category;
use App\Template;
use File;
class TemplateController extends Controller
{
	public function __construct()
    {
    	//$this->middleware('auth');
    	$this->middleware('auth', ['except' => ['getTemplate']]);
    }
    public function index()
    {
    	$path =public_path().'/frontend_asset/json/template/';
    	$file_name = str_replace("\\","/",File::files($path));
    	$category = Category::where('user_id',Auth()->user()->id)->get();
    	return view('frontend.template',compact('category','file_name'));
    }
    public function addNewCategory(Request $req){
    	$category = new Category();
    	$category->name = $req->name;
    	$category->description = $req->description;
    	$category->user_id = Auth()->user()->id;
    	$category->save();
    	return back();
    }
    public function getCategory(){
    	if(!Auth::check()){
    		return "You must login before!!";
    	}else{
    		return Category::where('user_id',Auth()->user()->id)->get();
    	}
    }
    public function addNewTemplate(Request $req){
    	if ($req->status) {
    		$content = (array) json_decode($req->content);
    		$data = json_encode($content["content"]);
    		$file_name = preg_replace('/\s+/', '',$req->name."-".Auth()->user()->id.'.json');
    		$path=public_path()."/frontend_asset/json/template";
    		if (!is_dir($path)) { File::makeDirectory($path, $mode = 0777, true, true);;  }
	      		File::put($path.'/'.$file_name,$data);
	    	}else{
	    		$template = new Template();
		    	$template->name = $req->name;
		    	$template->content = $req->content;
		    	$template->category_id = $req->category;
		    	$template->user_id = Auth()->user()->id;
		    	$template->save();
	    	}
    	return back();
    }
    public function getTemplate(Request $req){
    	return Template::where('id', $req->id)->first();
    }
    public function editTemplate(Request $req){
    	$a = (array) json_decode($req->content);
    	// echo "<pre>";
    	// print_r($req->all());
    	// die;
    	$template = Template::where('id',$req->id)->first();
    	$template->name = $req->name;
    	$template->content = $req->content;
    	$template->category_id = $req->category;
    	if($template->save()){
    		return back();
    	}
    }
}
