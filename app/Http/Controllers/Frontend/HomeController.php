<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use File;

class HomeController extends Controller
{
    function index(){
    	$path =public_path().'/frontend_asset/json/template/';
    	$file_name = str_replace("\\","/",File::files($path));
    	$category = Category::get();
    	return view('frontend.index',compact('category','file_name'));
    }
}
