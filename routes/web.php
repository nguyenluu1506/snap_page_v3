<?php

//backend
Route::prefix('admin')->group(function(){
  Route::get('/', 'Admin\AdminController@index')->name('admin');
});


//front_end
	/*get view*/
	//home
	Route::get('/','Frontend\HomeController@index')->name('frontend.home');
	Route::get('/my-template','Frontend\TemplateController@index')->name('template.home');
	Route::post('/add-category','Frontend\TemplateController@addNewCategory')->name('template.addNewCategory');
	Route::get('/get-category','Frontend\TemplateController@getCategory')->name('template.getCategory');
	Route::post('/save-template','Frontend\TemplateController@addNewTemplate')->name('template.addNewTemplate');
	Route::get('/get-template','Frontend\TemplateController@getTemplate')->name('template.getTemplate');
	Route::post('/edit-template','Frontend\TemplateController@editTemplate')->name('template.editTemplate');


 Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
