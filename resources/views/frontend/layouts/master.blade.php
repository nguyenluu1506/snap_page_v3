<!DOCTYPE html>
<html lang="en" ng-app="myApp">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <base href="{{asset('')}}">
    <title>Snap Page</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="shortcut icon" href="favicon.ico"/>
    <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.validate.min.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/jszip.min.js"></script>
    <script src="js/filesaver.js"></script>
</head><!--/head-->

<body>
  @include('frontend.layouts.partitals.header')
	@yield('content')
       
@if(Session::has('errors'))
    <script>
        var errors = "{!! $errors->first('email') ? $errors->first('email') : $errors->first('error_login') !!}";
        console.log(errors);
        $(document).ready(function() {
            $("#modal").modal('show');
            $(".modal-title").html('Login');
            $("#form").attr('action', 'login');
            $("#group").append(errors);
            $.get("frontend_asset/partital-views/login-form.html", function(data){
                $("#group").append(data);
            });
        });
    </script>
@endif           
            
            
    
    <script src="{{ mix('/js/app.js') }}"></script>
</body>
</html>
