<nav class="navbar navbar-default" role="navigation">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
				<a class="" style="margin-left:30px; margin-right: 45px;" href="./"> <img src="images/logo.png" width="70px"></a>
				<span id="control">
				<button class="btn btn-success" id="edit">Edit</button>
				<button class="btn btn-warning" id="preview">Preview</button>
				<button class="btn btn-danger" id="delete">Delete</button>
				<button class="btn btn-primary" id="save" onclick="save()">Save</button>

			</span>	
			<span id="controls"></span>
			<span id="name_template"></span>
		</div>
		
		
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse navbar-ex1-collapse">
			<ul class="nav navbar-nav " style="float: right;">
				@guest
                <li class="li"><a href="#modal" data-toggle="modal" id="register"><b>Register</b></a></li>
				<li class="li"><a href="#modal"  data-toggle="modal" id="login"><b>Login</b></a></li>
                @else
					<li class="li">
						<div class="dropdown" style="margin-top: 10px;">
				  			<button class="btn btn-success dropdown-toggle" type="button" data-toggle="dropdown"><b>Hello, {{ Auth::user()->name }}</b>
				  			<span class="caret"></span></button>
				  			<ul class="dropdown-menu">
						    	<li><a href="#"><b>Infor</b></a></li>
						    	<li><a href="{{ route('template.home') }}"><b>My Layout</b></a></li>
						    	<li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                       document.getElementById('logout-form').submit();"><b>Logout</b></a>
                                   <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                              {{ csrf_field() }}</li></form>
						  	</ul>
						</div>
					</li>
				@endguest
				
			
			</ul>
		</div><!-- /.navbar-collapse -->

	</div>
</nav>

<div class="modal fade" tabindex="-1" role="dialog" id="modal" >
    <div class="modal-dialog">
	<div class="modal-content">
	    <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h3 class="modal-title text-center"></h3>
	    </div>
	    <div class="modal-body">
			<form id="form" name="form" class="form-horizontal" method="POST">
				{{ csrf_field() }}
				<div id="group"></div>
			</form>
	    </div>
	</div>
    </div>
</div>

