<div class="sidebar-nav">
   <div class="edit-box"></div>
   
   <div class="edit-base">
		<div class="base-text">
			<label class="head">Text</label>

			<div class="content assignable font-family-size">
				<select class="base-text-item" name="font-family" title="font-family" placeholder="Font family">
				    <option value="unset" style="font-family: unset;">Unset</option>
				    <option value="Arial" style="font-family: Arial;">Arial</option>
				    <option value="Helvetica" style="font-family: Helvetica;">Helvetica</option>
				    <option value="Times New Roman" style="font-family: &quot;Times New Roman&quot;;">Times New Roman</option>
				    <option value="Courier New" style="font-family: &quot;Courier New&quot;;">Courier New</option>
				    <option value="Verdana" style="font-family: Verdana;">Verdana</option>
				    <option value="Georgia" style="font-family: Georgia;">Georgia</option>
				    <option value="Tahoma" style="font-family: Tahoma;">Tahoma</option>
				    <option value="Calibri" style="font-family: Calibri;">Calibri</option>
				    <option value="Garamond" style="font-family: Garamond;">Garamond</option>
				    <option value="Bookman" style="font-family: Bookman;">Bookman</option>
				</select>
				<input class="base-text-item px" type="number" name="font-size" placeholder="auto" value="" title="font-size" min="4" max="72" />
			</div>

			<div class="content">
				<div class="content col40 selectable select-many">
					<div class="base-text-item" data-attr-name="font-weight" data-attr-value="700" title="Bold">
		          		<span class="svg-container">
			                <svg version="1.1" width="13px" height="13px" viewBox="0 0 32 32">
								<path d="M22.121 15.145c1.172-1.392 1.879-3.188 1.879-5.145 0-4.411-3.589-8-8-8h-10v28h12c4.411 0 8-3.589 8-8 0-2.905-1.556-5.453-3.879-6.855zM12 6h3.172c1.749 0 3.172 1.794 3.172 4s-1.423 4-3.172 4h-3.172v-8zM16.969 26h-4.969v-8h4.969c1.827 0 3.313 1.794 3.313 4s-1.486 4-3.313 4z"></path>
							</svg>
		                </span>
		            </div>
		          	<div class="base-text-item" data-attr-name="font-style" data-attr-value="italic" title="Italic">
		                <span class="svg-container">
		                   <svg version="1.1" viewBox="0 0 305 305" style="enable-background:new 0 0 305 305;" xml:space="preserve" width="12px" height="12px">
								<polygon points="275,30 275,0 135,0 135,30 183.216,30 89.671,275 30,275 30,305 170,305 170,275 121.784,275 215.329,30 " data-original="#000000" class="active-path" data-old_color="#000000" fill="#414040"/>
							</svg>
		            	</span>
		            </div>
				</div>

				<div class="content col40 selectable select-one">
		            <div class="base-text-item" data-attr-name="text-decoration" data-attr-value="underline" title="Underline">
		                <span class="svg-container">
		                   <svg class="svg" width="12" height="12" xmlns="http://www.w3.org/2000/svg">
		                      <path d="M3 5.5V0H2v5.5C2 7.433 3.567 9 5.5 9 7.433 9 9 7.433 9 5.5V0H8v5.5C8 6.88 6.88 8 5.5 8 4.12 8 3 6.88 3 5.5zM0 12h12v-1H0v1z" fill-rule="evenodd"></path>
		                   </svg>
		                </span>
		            </div>
		            <div class="base-text-item" data-attr-name="text-decoration" data-attr-value="line-through" title="Strikethrough">
		                <span class="svg-container">
		                   <svg class="svg" width="12" height="12" xmlns="http://www.w3.org/2000/svg">
		                      <path d="M3.904 1.297C4.556.77 5.428.5 6.349.5c.907 0 1.756.228 2.407.742.661.523 1.069 1.301 1.149 2.295l-.997.08c-.06-.75-.353-1.26-.772-1.59C7.707 1.69 7.097 1.5 6.349 1.5c-.735 0-1.373.215-1.816.574-.434.352-.703.855-.703 1.503 0 .615.285 1.013.639 1.292.06.048.122.091.184.131h-1.41c-.246-.373-.413-.842-.413-1.423 0-.96.412-1.745 1.074-2.28zM8.824 8h1.069c.069.24.107.507.107.801 0 .972-.373 1.793-1.065 2.361-.68.56-1.622.838-2.707.838-1.1 0-1.986-.31-2.628-.879-.64-.567-.99-1.347-1.096-2.2l.992-.124c.083.66.343 1.2.767 1.575.422.374 1.055.628 1.965.628.925 0 1.619-.237 2.072-.61.442-.363.7-.891.7-1.589 0-.324-.066-.586-.177-.801zM0 7h12V6H0v1z" fill-rule="evenodd"></path>
		                   </svg>
		                </span>
		            </div>
		        </div>
			</div>

			<div class="content assignable">
			   <label title="Line Height">
			      <span class="">
			         <svg class="svg" width="14" height="14" xmlns="http://www.w3.org/2000/svg">
			            <path d="M14 1H0V0h14v1zm0 13H0v-1h14v1z"></path>
			            <path d="M3.548 11l2.8-8h1.304l2.8 8h-.954l-.7-2H5.202l-.7 2h-.954zM7 3.862L8.448 8H5.552L7 3.862z" fill-rule="evenodd"></path>
			         </svg>
			      </span>
			      <input class="base-text-item px" type="number" name="line-height" spellcheck="false" value="" placeholder="14" min="0">
			   </label>
			   <label title="Letter Spacing">
			      <span class="">
			         <svg class="svg" width="16" height="12" xmlns="http://www.w3.org/2000/svg">
			            <path d="M0 12V0h1v12H0zm15 0V0h1v12h-1z"></path>
			            <path d="M4.548 10l2.8-8h1.304l2.8 8h-.954l-.7-2H6.202l-.7 2h-.954zM8 2.862L9.448 7H6.552L8 2.862z" fill-rule="evenodd"></path>
			         </svg>
			      </span>
			      <input type="number" name="letter-spacing" spellcheck="false" class="base-text-item px" placeholder="0" min="0">
			   </label>
			</div>

			<div class="content assignable color">
			   	<label title="Line Height">
			      	<span class="content__label">color</span>
			      	<div id="colorpkr-font" class="base-text-item colorpkr" data-attr-name="color">
		            	<div></div>
		            </div>
			   	</label>
			   	<label title="Letter Spacing">
			      	<span class="content__label">bg</span>
			      	<div id="colorpkr-background" class="base-text-item colorpkr" data-attr-name="background-color">
		            	<div></div>
		            </div>
			   	</label>
			</div>
		</div>

        <div class="base-align-horizontal">
			<label class="head">Alignment</label>
			<div class="content selectable select-one">
				 <div class="base-text-item" data-attr-name="text-align" data-attr-value="left" title="text-align-left">
				    <span class="svg-container">
				       <svg class="svg" width="14" height="10" xmlns="http://www.w3.org/2000/svg">
				          <path d="M0 0h14v1H0V0zm0 4h8v1H0V4zm10 4H0v1h10V8z" fill-rule="evenodd"></path>
				       </svg>
				    </span>
				 </div>
				 <div class="base-text-item" data-attr-name="text-align" data-attr-value="center" title="text-align-center">
				    <span class="svg-container">
				       <svg class="svg" width="14" height="10" xmlns="http://www.w3.org/2000/svg">
				          <path d="M0 0h14v1H0V0zm3 4h8v1H3V4zm9 4H2v1h10V8z" fill-rule="evenodd"></path>
				       </svg>
				    </span>
				 </div>
				 <div class="base-text-item" data-attr-name="text-align" data-attr-value="right" title="text-align-right">
				    <span class="svg-container">
				       <svg class="svg" width="14" height="10" xmlns="http://www.w3.org/2000/svg">
				          <path d="M0 0h14v1H0V0zm6 4h8v1H6V4zm8 4H4v1h10V8z" fill-rule="evenodd"></path>
				       </svg>
				    </span>
				 </div>
				 <div class="base-text-item" data-attr-name="text-align" data-attr-value="justify" title="text-align-justified">
				    <span class="svg-container">
				       <svg class="svg" width="14" height="10" xmlns="http://www.w3.org/2000/svg">
				          <path d="M0 0h14v1H0V0zm0 4h14v1H0V4zm14 4H0v1h14V8z" fill-rule="evenodd"></path>
				       </svg>
				    </span>
				 </div>
			</div>
        </div>

        <div class="base-transform">
        	<label class="head">Transform</label>

			<div class="content selectable select-one">
	             <div class="base-text-item" data-attr-name="text-transform" data-attr-value="uppercase" title="Uppercase">
	                <span class="svg-container">
	                   <svg class="svg" width="14" height="8" xmlns="http://www.w3.org/2000/svg">
	                      <path d="M.049 8l2.8-8h1.303l2.8 8H6l-.7-2H1.702l-.7 2H.05zM3.5.862L4.949 5H2.052L3.501.862zM10.89 8h-2.89V0h2.796c1.672 0 2.454.953 2.454 2.125 0 1.031-.61 1.484-1.297 1.672v.078c.734.047 1.61.734 1.61 2C13.563 7.078 12.781 8 10.89 8zm-1.89-3.64V7h1.921c1.079 0 1.624-.422 1.624-1.125 0-.813-.561-1.516-1.608-1.516H9.001zM9 1v2.516h1.765c.875 0 1.51-.547 1.51-1.391 0-.703-.44-1.125-1.447-1.125H9z" fill-rule="evenodd"></path>
	                   </svg>
	                </span>
	             </div>
	             <div class="base-text-item" data-attr-name="text-transform" data-attr-value="lowercase" title="Lowercase">
	                <span class="svg-container">
	                   <svg class="svg" width="12" height="9" xmlns="http://www.w3.org/2000/svg">
	                      <path d="M2.159 8.14c1.047 0 1.593-.562 1.78-.953h.048V8h.922V4.047c0-1.906-1.453-2.125-2.22-2.125-.905 0-1.937.312-2.405 1.406l.875.313c.203-.438.683-.907 1.562-.907.856 0 1.266.457 1.266 1.235 0 .445-.453.422-1.547.562-1.113.145-2.328.39-2.328 1.766 0 1.172.906 1.844 2.047 1.844zm.14-.828c-.734 0-1.265-.328-1.265-.968 0-.703.64-.922 1.36-1.016.39-.047 1.437-.156 1.593-.344v.844c0 .75-.594 1.484-1.688 1.484zM6.591 8h.89v-.922h.11c.203.328.594 1.047 1.75 1.047 1.5 0 2.547-1.203 2.547-3.11 0-1.89-1.047-3.093-2.562-3.093-1.172 0-1.532.719-1.735 1.031h-.078V0h-.922v8zm.907-3c0-1.344.593-2.25 1.718-2.25 1.172 0 1.75.984 1.75 2.25 0 1.281-.593 2.297-1.75 2.297-1.11 0-1.718-.938-1.718-2.297z"></path>
	                   </svg>
	                </span>
	             </div>
	             <div class="base-text-item" data-attr-name="text-transform" data-attr-value="capitalize" title="Capitalize">
	                <span class="svg-container">
	                   <svg class="svg" width="14" height="9" xmlns="http://www.w3.org/2000/svg">
	                      <path d="M8.97 8H8V0h1v2.953h.079l.047-.074c.212-.339.6-.957 1.687-.957 1.516 0 2.563 1.203 2.563 3.094 0 1.906-1.047 3.109-2.547 3.109-1.109 0-1.514-.66-1.724-1.004l-.026-.043h-.11V8zm1.734-5.25c-1.125 0-1.719.906-1.719 2.25 0 1.36.61 2.297 1.719 2.297 1.156 0 1.75-1.016 1.75-2.297 0-1.266-.578-2.25-1.75-2.25zM.049 8l2.8-8h1.303l2.8 8H6l-.7-2H1.702l-.7 2H.05zM3.5.862L4.949 5H2.052L3.501.862z" fill-rule="evenodd"></path>
	                   </svg>
	                </span>
	             </div>
	        </div>
        </div>

        <div class="base-border">
        	<label class="head">Border</label>

			<div class="content assignable">
	            <input type="number" name="border-width" class="col30 base-text-item px" placeholder="0" min="0">
                <select id="style-border" name="border-style" class="col40 base-text-item">
                	<option value="unset">None</option>
                	<option value="solid">────</option>
                	<option value="dashed">- - - - -</option>
                	<option value="dotted">..........</option>
                </select>
	            <div id="colorpkr-border" class="col30 base-text-item colorpkr" data-attr-name="border-color">
	            	<div></div>
	            </div>
	        </div>
        </div>

        <div class="base-margin">
			<label class="head">Margin</label>
			<div class="content assignable">
				<label title="margin-left">
					<span class="content__label">Left </span>
					<input class="base-text-item px" type="number" name="margin-left" spellcheck="false" value="" placeholder="0">
				</label>
				<label title="margin-top">
					<span class="content__label">Top </span>
					<input type="number" name="margin-top" spellcheck="false" class="base-text-item px" placeholder="0">
				</label>
			</div>
			<div class="content assignable">
				<label title="margin-right">
					<span class="content__label">Right </span>
					<input class="base-text-item px" type="number" name="margin-right" spellcheck="false" value="" placeholder="0">
				</label>
				<label title="margin-bottom">
					<span class="content__label">Bottom </span>
					<input type="number" name="margin-bottom" spellcheck="false" class="base-text-item px" placeholder="0">
				</label>
			</div>
        </div>

        <div class="base-padding">
			<label class="head">Padding</label>
			<div class="content assignable">
				<label title="padding-left">
					<span class="content__label">Left </span>
					<input class="base-text-item px" type="number" name="padding-left" spellcheck="false" value="" placeholder="0">
				</label>
				<label title="padding-top">
					<span class="content__label">Top </span>
					<input type="number" name="padding-top" spellcheck="false" class="base-text-item px" placeholder="0">
				</label>
			</div>
			<div class="content assignable">
				<label title="padding-right">
					<span class="content__label">Right </span>
					<input class="base-text-item px" type="number" name="padding-right" spellcheck="false" value="" placeholder="0">
				</label>
				<label title="padding-bottom">
					<span class="content__label">Bottom </span>
					<input type="number" name="padding-bottom" spellcheck="false" class="base-text-item px" placeholder="0">
				</label>
			</div>
        </div>
   </div>
</div>