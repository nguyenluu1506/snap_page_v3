<div class="sidebar-nav">
	<ul class="nav nav-list flex-column">
		<li class="nav-item nav-header">
			Grid System
		</li>
		<li class="rows left-item">
			<div class="lyrow">
				<input type="text" value="12">
				<label class="btn btn-primary drag">drag</label>
			</div>
			<div class="lyrow">
				<input type="text" value="9 3">
				<label class="btn btn-primary drag">drag</label>
			</div>
			<div class="lyrow">
				<input type="text" value="6 6">
				<label class="btn btn-primary drag">drag</label>
			</div>

			<div class="lyrow">
				<input type="text" value="" placeholder="Custom col">
				<label class="btn btn-primary drag" style="display: none;">drag</label>
			</div>
		</li>
	</ul>
	<hr>
	<ul class="nav nav-list flex-column">
		<li class="nav-item nav-header">
			Base Css
		</li>
		<li class="boxes base-css left-item">
			<!-- list tag get from json file here -->
		</li>
	</ul>
	<hr>

	<ul class="nav nav-list flex-column">
		<li class="nav-item nav-header">
			Template Demo
		</li>
		<li class="boxes left-item">
			@foreach($file_name as $name)
			<?php $arr = explode('/', $name); $file = $arr[sizeof($arr)-1]?>
			<div class="item-user" value="{{ $file }}">
				<span>{{ str_replace('.json', '', $file) }}</span>
				<label class="btn btn-primary drag">drag</label>
			</div>
			@endforeach
		</li>
	</ul>

</div>