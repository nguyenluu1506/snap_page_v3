@extends('frontend.layouts.master')
@section('content')
<div class="wrapper">
	<div class="left">
		<div class="sidebar-nav">
			<h4 style="color: #fff"><center>My Layout</center></h4>
			<ul class="nav nav-list flex-column">
				@foreach($category as $cate)
				<li class="nav-item nav-header">
					{{ $cate->name }}
				</li>
				<li class="boxes">
					<div class="" style="display: block !important;">
						@foreach($cate->Layout as $temp)
						<a href="javascript:void(0);" onclick="showTemplate({{ $temp->id }})" style="text-decoration: none; color: #fff;">{{ $temp->name }}</a><br>
						@endforeach
					</div>
				</li>
				@endforeach
				<li><a href="javascript:void(0);" class="btn btn-success" onclick="addCategory()"> <strong>+ Add new category</strong></a></li>
			</ul>
		</div>
		<hr>
		@include('frontend.layouts.partitals.sidebar')
	</div>
	<div class="content">
		<div id="loader"></div>
		<div id="sortable-area2" class="group">
		</div>
	</div>
	<div class="right">
		@include('frontend.layouts.partitals.edit')
	</div>
</div>
@endsection