@extends('frontend.layouts.master')
@section('content')
<div class="wrapper">
	<div class="left">
		@include('frontend.layouts.partitals.sidebar')
	</div>
	<div class="content">
		<div id="sortable-area2" class="group">
		</div>
	</div>
	<div class="right">
		@include('frontend.layouts.partitals.edit')
	</div>
</div>
@endsection