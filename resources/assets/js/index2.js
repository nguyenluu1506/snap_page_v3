
window.object = {
	"tag" : "div",
	"category" : "grid",
	"attr" : {
		"id": "sortable-area2",
		"class" : "row group",
		"style" : {}
	},
	"content" : []
};


import element from './components/element';

$(document).ready(function() {
	var ele = new element;
	var w   = window;
	$.ajax({
		url: './frontend_asset/json/process.php',
		data: {listdata: "listdata"},
		type: "POST",
		success: function(response) {
			getFileName(JSON.parse(response));
			ele.sortableArea();
		},
		error : (response) => {
			console.log("error : ",response);
		}
	});

	function getFileName(response) {
		$.each(response, (k,v) => {
			if(v != "." && v != "..") {
				let name = getFileName.prototype.detachName(v);
				let html=`
					<div class="box" value="${name}.json">
						<span>${name}</span><label class="btn btn-primary drag">drag</label>
					</div>
				`;
				$('.left .base-css').append(html);
			}
		});
	}

	getFileName.prototype.detachName = function(name){
		let arr = name.split('.');
		let tagName = arr[0];
		return tagName;
	};

	// validate input creat grid layout
	$('.lyrow input').bind('keyup', validateInputGrid);

	function validateInputGrid() {
		let sum  = 0;
		let cols = $(this).val().split(" ",12);
		$.each(cols, function(index,value){
			sum = sum + parseInt(value);
		});
		if(sum==12) {
			$(this).parent().find('label').show();
		} else {
			$(this).parent().find('label').hide();
		}
	}

	// button remove element
	$('body').on('click', '.content .remove', function() {
		let el = $(this).parent().parent();
		ele.removeNodeObject(el);
		$(el).remove();
		$('.edit-box').empty();
		$('.right').hide();
	});

	// animation sidebar
	$('.left-item').hide();
	$('.nav-header').click(function(){
		$('.left-item').hide();
		$(this).next().slideToggle();
	});


	// edit element
	$('.right').hide();
	$('body').on('click', '.content .edit-el', function() {
		$('.right').show();
		$('.right .selected').removeClass('selected');
		$('.right .assignable .base-text-item').val('');
		let category = $(this).parent().parent().data("category");
		let box      = $('.right .edit-box');
		box.empty();
		let el = null;
		let obj = null;
		if(category == "grid") {
			el  = $(this).parent().parent()[0];
			obj = ele.getParent($(this).parent());
		} else {
			el  = $(this).parent().siblings()[0];
			obj = ele.getParent(el).content[0];
		}
		$('.active').removeClass('active');
		$(el).addClass('active');

		w.el  = null;
		w.obj = null;
		w.el  = el;
		w.obj = obj;

		ele.fillStyleInBox();
		ele.editBaseStyle();
		$.ajax({
			url: "./frontend_asset/json/categories/" + category + ".json",
    		dataType: "json",
    		success: (response) => {
    			createBoxStyle(response, el, box);
    			ele.editElement(el, obj);
    			readImage(el, obj);
			}, 
			error : (response) => {
				console.log("error : ",response);
			}
		});

		function readImage(el, obj) {
			$('.right input[type="file"]').change(function() {
				let reader = new FileReader();
				reader.onload = function (e) {
					$(el).attr('src', e.target.result);
				};
				reader.readAsDataURL(this.files[0]);
				let src = $(this).val();
				let arr = src.split("\\");
				src = arr[arr.length-1];
				obj.attr.src = "./images/" + src;
				pushImage();
			});
		};

		function pushImage() {
			var myFormData = new FormData();
			myFormData.append("file", $('.right input[type="file"]').prop('files')[0]);
			$.ajax({
		        type: 'POST',               
		        processData: false, // important
		        contentType: false, // important
		        data: myFormData,
		        url: "./frontend_asset/images/upload.php",
		        success: function(data){
		            console.log(data);
		        }
		    });
		};
	});
 
	// fill style category in box design
	var flag = 0;
	function createBoxStyle(json, el, box) {
		flag++;
		let style = window.getComputedStyle(el);
		$.each(json, (k,v)=> {
			let label = document.createElement('label');
			let div   = document.createElement('div');
			let pxPattern = ['width', 'height'];
			$(label).html(k);
			$(div).append(label);
			if(typeof(v) == "object") {
				if(k == "style") {
					createBoxStyle(v, el, box);
				} else {
					var select = document.createElement('select');
					(flag%2 != 0)? $(select).attr('data-type', 'attr') : $(select).attr('data-type', 'css');
					$(select).attr('name', k);
					$.each(v, (j,i) => {
						let option = document.createElement('option');
						$(option).attr('value', i);
						$(option).css(k, i);
						$(option).html(i);
						$(select).append(option);
					});
					$(div).append(select);
					$(box).append(div);
				}
			} else {
				let input = document.createElement('input');
				(flag%2 != 0)? $(input).attr('data-type', 'attr') : $(input).attr('data-type', 'css');
				$(input).attr('name', k);
				if(pxPattern.indexOf(k) > -1) {
					$(input).attr('type', 'number');
				}
				if (k == "src") {
					$(input).attr('type', 'file');
				}
				$(input).attr('placeholder',style.getPropertyValue(k));
				$(div).append(input);
				$(box).append(div);
			}
		});
	}

	// privew page
	$('#preview').click(function(){
		$('.left').hide();
		$('.right').hide();
		$('.content').addClass('sourcepreview');
		$('.active').removeClass('active');
	});

	// edit page
	$('#edit').click(function(){
		$('.left').show();
		$('.content').removeClass('sourcepreview');
	});

	// delete all element created in page
	$('#delete').click(function(){
		if(confirm("Are you sure?")) {
			$('#sortable-area2').empty();
			$('.edit-box').empty();
			object.content = [];
		}
	});

	$('body').on('keyup', '#sortable-area2', function(e) {
		let objParent = ele.getParent(e.target);
		let obj;
		$.each(objParent.content, (k,v) => {
			if(v.tag.toUpperCase() == e.target.tagName) {
				obj = v;
				return false;
			}
		});
		let arr = ['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'p', 'a', 'small', 'span', 'strong', 'b', 'em', 'i'];
		if(arr.indexOf(obj.tag) > -1) obj.content = e.target.innerText;
	})
});