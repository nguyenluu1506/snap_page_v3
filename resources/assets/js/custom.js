import element from './components/element';

var ele = new element;

window.login=function(){
        $(".modal-title").html('Login');
        $("#form").attr('action', 'login');
        $.get("frontend_asset/partital-views/login-form.html", function(data){
        	$("#group").html(data);
    	});
}
window.register=function(){
        $(".modal-title").html('Register');
        $("#form").attr('action', 'register');
        $.get("frontend_asset/partital-views/register-form.html", function(data){
        	$("#group").html(data);
    	});
    }

$(document).ready(function() {
    $('#login').click(function() {
    	login();
    });
    $('#register').click(function() {
    	register();
    });
});

window.exportZip = function(){
	var html_content = '';
	var bootstrap_css = "";
	var bootstrap_js = "";
	var jquery = "";
	getData();
	function getData(){
		$.ajax({
			url: "./frontend_asset/partital-views/head-html-file.html",
			success: (response) => {
				html_content += response;
				$("#sortable-area2 .content__control").remove();
				html_content +=  $("#sortable-area2")[0].innerHTML;
				html_content += "</body></html>";
				
				$.ajax({
					url: "./css/bootstrap.min.css",
					success: (response) => {
						bootstrap_css += response;

						$.ajax({
							url: "./js/bootstrap.min.js",
							success: (response) => {
								bootstrap_js += response;

								$.ajax({
									url: "./js/jquery-1.12.4.min.js",
									success: (response) => {
										jquery += response;
										zip(html_content, bootstrap_css, jquery, bootstrap_js);
									}, 
									error : (response) => {
										alert('có lỗi xảy ra!');
									}
								});
							}, 
							error : (response) => {
								alert('có lỗi xảy ra!');
							}
						});

					}, 
					error : (response) => {
						alert('có lỗi xảy ra!');
					}

				});
			}, 
			error : (response) => {
				alert('có lỗi xảy ra!');
			}
		});
	}
   	
    
 
	function zip(html_content, bootstrap_css, jquery, bootstrap_js){
		html_content=html_content.replace(/contenteditable\=\"true\"/gi,'');
	   	var zip = new JSZip();
		zip.file("css/bootstrap.min.css", bootstrap_css);
		zip.file("js/jquery.min.js", jquery);
		zip.file("js/bootstrap.min.js", bootstrap_js);
		zip.file("index.html", html_content);
		var file_name = Math.floor(Math.random() * 10000)+"_snap-page.zip";
		zip.generateAsync({type:"blob"})
		.then(function (blob) {
		    saveAs(blob, file_name);
		});
	}
};

window.addCategory = function(){
	$("#modal").modal('show');
	$(".modal-title").html('Add new Category');
    $("#form").attr('action', 'add-category');
    $.get("frontend_asset/partital-views/add-category-form.html", function(htm){
        $("#group").html(htm);
    });
};

window.showTemplate = function(id){
	$("#loader").html('<div class="loader" style="margin-top:100px;"></div>');
	$("#control").css('display', '');
	$.ajax({
      	url: 'get-template',
      	data : {'id':id},
      	dataType:'json',
      	type: 'get',   
	    success: function(data){ 
	    	object = JSON.parse(data.content);
	    	var option = "";
	    	$("#save").attr('onclick', 'saveEdit()');
	    	$.ajax({
		      	url: 'get-category', 
		      	type: 'get',   
			    success: function(datas){ 
			    	for (var i = 0; i < datas.length; i++) {
			    	 	option += '<option value="'+datas[i].id+'">'+datas[i].name+'</option>';
			    	}
					$(".modal-title").html('Save layout');
					$("#group").css('height', 'auto');
			        $("#form").attr('action', 'edit-template');
			        $.get("frontend_asset/partital-views/edit-form.html", function(htm){
		                $("#group").html(htm);
		                $("#id_layout").val(data.id); 
		                $("#name").val(data.name);
		            });
	      		}
	    	});
	    	$("#name_template").html("<b id='name-template' style='border-radius:15px;color:#fff; padding:5px; background:orange;'>Editting for:  '"+data.name+"' Template <a href='javascript:void(0);' style='color:red;' onclick='swap()'>X</a></b>");
			$('#loader').hide();
			$(".content #sortable-area2").empty();	    	
	    	$.each(object.content, (k,v) => {
	    		$(".content #sortable-area2").append(ele.draw(v));
	    	});
	    	// register event dreg and sortable for data
	    	ele.sortableArea();
	    	ele.sortableColumn();
  		}
	});
};

window.swap = function(){
	$("#name-template").hide();
	$("#save").attr('onclick', 'save()');
	$("#sortable-area2").empty();
	$('.right').hide();
	object.content = [];
}

window.saveEdit = function(data){
	var content = JSON.stringify(object);
	$("#modal").modal('show');
	$(".modal-title").html("Save Template");
	$("#group").append('<input class="decorative-input inspectletIgnore form-control" value=\''+content+'\' placeholder="" name="content" style="display: none;">');
}

$('body').on('click', '#save_template', function() {
	$(this).parent().append('<input type="text" value="1" name="status">');
});

//save
window.save = function(){
	if ($("body #sortable-area2").children().length > 0) {
		var option = "";
		var content = JSON.stringify(object);
		$("#modal").modal('show');
		$("#group").css('height', '60px');
		$("#group").html('<div class="loader col-md-6 col-md-offset-5"></div>');
		$("#group").css('height', 'auto');
		$.ajax({
	      	url: 'get-category', 
	      	type: 'get',   
		    success: function(data){ 
		    	for (var i = 0; i < data.length; i++) {
		    	 	option += '<option value="'+data[i].id+'">'+data[i].name+'</option>';
		    	}
				$(".modal-title").html('Save layout');
		        $("#form").attr('action', 'save-template');
		        $.get("frontend_asset/partital-views/add-new.html", function(data){
	                $("#group").html(data);
	                $("#content").val(content); 
	            });
      		},error: function(data){
	            $(".modal-title").html('Login');
	            $("#form").attr('action', 'login');
	            $.get("frontend_asset/partital-views/login-form.html", function(data){
	                $("#group").html(data);
	            });
      		}
    	});
	}else{alert("Nothing to save!");}
}

$('body').on('click', '#save_template', function() {
	$(this).parent().append('<input type="text" value="1" name="status">');
});


$("#form").validate({
	onfocusout: false,
	onkeyup: false,
	onclick: false,
	rules: {
		"name": {
			required: true
		},
		"email": {
			required: true
		},
		"password": {
			required: true,
			minlength: 6
		},
		"re_password": {
			required: true,
			equalTo: "#password",
			minlength: 6
		},
		"description" : {
			required: true
		},
		"category" : {
			required: true
		}
	},
	messages: {
		"name": {
			required: "This field is required!"
		},
		"email":{
			require: "This field is required!"
		},
		"password": {
			required: "This field is required!",
			minlength: "Min length 6 digits"
		},
		"re_password": {
			required: "This field is required!",
			equalTo: "Two passwords must be same!",
			minlength: "Min length 6 digits"
		},
		"description" : {
			required: "This field is required!"
		},
		"category" : {
			required: "Please select an item!",
		}
	}
});