function objectJson() {}

objectJson.prototype.draw = function(json) {
	let tag = document.createElement(json.tag);
	if(json.category != "") 
		$(tag).attr('data-category', json.category);
	if(json.attr != null) {
		$.each(json.attr, (k,v) => {
			if(typeof(v) != "string") {
				if(k == "style") {
					$.each(v, (j,i) => {
						$(tag).css(j,i);
					})
				}
			} else {
				$(tag).attr(k, v);
			}
		});
	}
	if(typeof(json.content) != "string") {
		$.each(json.content, (k,v)=>{
			tag.appendChild(objectJson.prototype.draw(v));
		})
	} else {
		$(tag).html(json.content);
	}
	return tag;
};

objectJson.prototype.control = function() {
	let control  = {
		"tag" : "div",
		"category": "control",
		"attr" : {
			"class" : "content__control",
			"style": {}
		},
		"content" : [
			{
				"tag" : "label",
				"attr" : {
					"class" : "edit-el btn btn-warning",
					"style": {}
				},
				"content" : "edit"
			},
			{
				"tag" : "label",
				"attr" : {
					"class" : "drag btn btn-primary",
					"style": {}
				},
				"content" : "drag"
			},
			{
				"tag" : "label",
				"attr" : {
					"class" : "remove btn btn-danger",
					"style": {}
				},
				"content" : "remove"
			}
		]
	};
	return control;
};

export default objectJson;