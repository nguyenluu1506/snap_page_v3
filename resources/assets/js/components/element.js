import objectJson from './objectJson.js';

function element() {};

var w = window;

element.prototype = new objectJson();

element.prototype.dragdrop = function(el, obj) {
	if($(el.item).hasClass('box')) {
		let file = $(el.item).attr('value');		
        $.ajax({
    		url: "./frontend_asset/json/childs/" + file,
    		dataType: "json",
    		success: (response) => {
    			if(el.helper != null) {
    				obj.content.splice(el.helper.index(), 0, response);
    			}
    			response.content.push(element.prototype.control());
    			$(el.helper).replaceWith(element.prototype.draw(response));
			}, 
			error : (response) => {
				console.log("error : ",response);
			}		    		
    	});
    } else if($(el.item).hasClass('item-user')) {
    	let file = $(el.item).attr('value');		
        $.ajax({
    		url: "./frontend_asset/json/template/" + file,
    		dataType: "json",
    		success: (response) => {
    			let parentTag = $(el.helper).parent();
    			let temp = el.helper;
    			$.each(response, (k,v) => {
    				obj.content.splice(el.helper.index()+k, 0, v);
    				let tag = element.prototype.draw(v);
    				$(temp).after(tag);
    				temp = tag;
    			});
    			$(el.helper).remove();
    			element.prototype.sortableColumn();
    			element.prototype.sortableArea();
			}, 
			error : (response) => {
				console.log("error : ",response);
			}		    		
    	});
    } else {
    	let ele = $(el.item).children().first()[0];
    	let grid = element.prototype.createJsonGrid(ele);
    	grid.content.push(element.prototype.control());
    	if(el.helper != null) {
			obj.content.splice(el.helper.index(), 0, grid);
		}
		$(el.helper).replaceWith(element.prototype.draw(grid));
    }
};

element.prototype.createJsonGrid = function(el) {
	let cols = $(el).val().split(" ",12);
	let obj = {
		"tag" : "div",
		"category" : "grid",
		"attr" : {
			"class" : "row",
			"style" : {}
		},
		"content" : []
	};
	$.each(cols, function(index,value){
		let col = {
			"tag" : "div",
			"category" : "grid",
			"attr" : {
				"sort": "" + index,
				"class" : "col-md-" + value + " column",
				"style" : {}
			},
			"content" : []
		};
		obj.content.push(col);
	});
	return obj;
};

element.prototype.getParent = function(el) {
	if($(el).parent().attr("id") == "sortable-area2") {
		return object;
	} else {
		//duyet de tim vi tri
		let arr = [];
		$.each($(el).parents(), (k, v) => {
			arr.push($(v).index());
			if($(v).hasClass('group')) return false;
		});
		arr.pop();
		arr = arr.reverse();
		let root = window.object;
		$.each(arr, (j,i) => {
			root = root.content[i];
		})
		return root;
	}
};

element.prototype.removeNodeObject = function(el) {
	let objParent = element.prototype.getParent(el);
	let index = $(el).index();
	objParent.content.splice(index, 1);
};

element.prototype.editElement = function(el, obj) {
	$.each($('.edit-box select, .edit-box input'), (k,v) => {
		$(v).on({
			keyup: edit,
			change: edit
		});
	});

	function edit() {
		let name  = $(this).attr('name');
		let value = $(this).val();
		if($(this).attr('type') == 'number') value = value + 'px';
		if($(this).data('type') == "attr") {
			$(el).attr(name, value);
			$(this).val() == "" ? delete obj.attr[name] : obj.attr[name] = value;
		}else {
			$(el).css(name, value);
			$(this).val() == "" ? delete obj.attr.style[name] : obj.attr.style[name] = value;
		}
	}
};

element.prototype.editBaseStyle = function(el, obj) {
	$('.right .selectable .base-text-item').click(function() {
		if($(this).parent().hasClass('select-one')) {
			if($(this).hasClass('selected')) {
				$(this).toggleClass('selected');
			} else {
				$(this).parent().find('.selected').removeClass('selected');
				$(this).addClass('selected');
			}				
		} else {
			$(this).toggleClass('selected');
		}

		let name  = $(this).data('attr-name');
		let value = $(this).data('attr-value');
		if($(this).hasClass('selected')) {
			$(w.el).css(name, value);
			w.obj.attr.style[name] = value;
		} else {
			$(w.el).css(name, '');
			delete w.obj.attr.style[name];
		}
	});

	$('.right .assignable .base-text-item').on({
		keyup: edit,
		change: edit
	});

	function edit() {
		let name  = $(this).attr('name');
		let value = $(this).val();
		if($(this).hasClass('px')) value = value + 'px';
		$(w.el).css(name, value);
		$(this).val() == "" ? delete w.obj.attr.style[name] : w.obj.attr.style[name] = value;
	};

	$.each($('#colorpkr-border, #colorpkr-font, #colorpkr-background'), (k,v) => {
		let style = $(v).data('attr-name');
		colorpkr(v, style);
	})

	function colorpkr(div, styleName) {
		$(div).ColorPicker({
			onShow: function (colpkr) {
				$(colpkr).fadeIn(500);
				return false;
			},
			onHide: function (colpkr) {
				$(colpkr).fadeOut(500);
				return false;
			},
			onChange: function (hsb, hex, rgb) {
				$(div).find('div').css('background', '#' + hex);
				$(w.el).css(styleName, '#' + hex);
				w.obj.attr.style[styleName] = '#' + hex;
			}
		});
	}
}

element.prototype.fillStyleInBox = function() {
	let style = window.getComputedStyle(w.el);
	$.each($('.right .selectable .base-text-item'), (k,v) => {
		let attr          = $(v).data('attr-name');
		let value         = $(v).data('attr-value');
		let response      = style.getPropertyValue(attr);
		let formatRespone = response.substring(0, response.indexOf(' '));

		if(response == value || formatRespone == value) {
			$(v).addClass('selected');
		}
	});

	$.each($('.right .assignable .base-text-item'), (k,v) => {
		let attr = $(v).attr('name');
		$(v).val(style.getPropertyValue(attr).replace('px',''));
	});
}

element.prototype.sortableColumn = function() {
	$('#sortable-area2 .column').sortable({
		connectWith: '.column, #sortable-area2',
		handle: '.drag',
		scroll: false,
		/*active: function(e, ui) {
			if($(ui.item).parents().prop("tagName") == "DIV") {
				console.log('column active', ui);
				indexStart = ui.item.index();
				nodeRemove = element.prototype.getParent(ui.helper);
			}
		},*/
		start: function(e, ui) {
			if($(ui.item).parents().prop("tagName") == "DIV") {
				// console.log('column start', ui);
				w.indexStart = ui.item.index();
				w.nodeRemove = element.prototype.getParent(ui.helper);
			}
		},
	    stop: function(e, ui) {
	    	w.indexStop = $(ui.item).index();
			if(indexStop != -1 && !$(ui.item).hasClass('box ui-draggable') && !$(ui.item).hasClass('item-user ui-draggable')) {
				// console.log('column stop', ui);
	        	w.item = nodeRemove.content.splice(indexStart, 1);
	        	w.nodeReceive = element.prototype.getParent(ui.item);
				w.nodeReceive.content.splice(indexStop, 0, item[0]);
			}
	    },
	    receive: function(e, ui) {
	    	if($(ui.item).parent().prop("tagName") == "LI") {
	    		// console.log('column receive', ui);
				let tree = element.prototype.getParent(ui.helper);
				element.prototype.dragdrop(ui,tree);
			}
	    }
	});
};

element.prototype.sortableArea = function() {
	$('body .sidebar-nav .box, body .sidebar-nav .item-user').draggable({
		connectToSortable: "#sortable-area2, .column",
		helper: "clone",
		handle: ".drag",
		scroll: false
	});

	$('body .sidebar-nav .lyrow').draggable({
		connectToSortable: "#sortable-area2, .column",
		helper: "clone",
		handle: ".drag",
		scroll: false,
		stop: function(e, ui) {
			element.prototype.sortableColumn();
		}
	});

	$('#sortable-area2').sortable({
		connectWith: '.column',
		handle: ".drag",
		scroll: false,
		start: function(e, ui) {
			if($(ui.item).parents().prop("tagName") == "DIV") {
				// console.log('root start', ui);
				w.indexStart = ui.item.index();
				w.nodeRemove = element.prototype.getParent(ui.helper);
			}
		},/*
		active: function(e, ui) {
			if($(ui.item).parents().prop("tagName") == "DIV") {
				console.log('root active', ui);
				indexStart = ui.item.index();
				nodeRemove = element.prototype.getParent(ui.helper);
			}
		},*/
		stop: function(e, ui) {
			w.indexStop = $(ui.item).index();
			if(indexStop != -1 && !$(ui.item).hasClass('box ui-draggable') && !$(ui.item).hasClass('item-user ui-draggable')) {
				// console.log('root stop', ui);
				w.item = nodeRemove.content.splice(indexStart, 1);
				w.nodeReceive = element.prototype.getParent(ui.item);
				w.nodeReceive.content.splice(indexStop, 0, item[0]);
			}
		},
		receive: function(e, ui){
			if($(ui.item).parent().prop("tagName") == "LI") {
				// console.log("root receive", ui);
				let tree = element.prototype.getParent(ui.helper);
				element.prototype.dragdrop(ui,tree);
			}
    	}
	});
};

export default element;