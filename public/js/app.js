/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/assets/js/app.js":
/*!************************************!*\
  !*** ./resources/assets/js/app.js ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
__webpack_require__(/*! ./components/colorpicker */ "./resources/assets/js/components/colorpicker.js");

__webpack_require__(/*! ./index2 */ "./resources/assets/js/index2.js");

__webpack_require__(/*! ./custom */ "./resources/assets/js/custom.js");

/***/ }),

/***/ "./resources/assets/js/components/colorpicker.js":
/*!*******************************************************!*\
  !*** ./resources/assets/js/components/colorpicker.js ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 *
 * Color picker
 * Author: Stefan Petre www.eyecon.ro
 * 
 * Dual licensed under the MIT and GPL licenses
 * 
 */
(function ($) {
  var ColorPicker = function () {
    var ids = {},
        inAction,
        charMin = 65,
        visible,
        tpl = '<div class="colorpicker"><div class="colorpicker_color"><div><div></div></div></div><div class="colorpicker_hue"><div></div></div><div class="colorpicker_new_color"></div><div class="colorpicker_current_color"></div><div class="colorpicker_hex"><input type="text" maxlength="6" size="6" /></div><div class="colorpicker_rgb_r colorpicker_field"><input type="text" maxlength="3" size="3" /><span></span></div><div class="colorpicker_rgb_g colorpicker_field"><input type="text" maxlength="3" size="3" /><span></span></div><div class="colorpicker_rgb_b colorpicker_field"><input type="text" maxlength="3" size="3" /><span></span></div><div class="colorpicker_hsb_h colorpicker_field"><input type="text" maxlength="3" size="3" /><span></span></div><div class="colorpicker_hsb_s colorpicker_field"><input type="text" maxlength="3" size="3" /><span></span></div><div class="colorpicker_hsb_b colorpicker_field"><input type="text" maxlength="3" size="3" /><span></span></div><div class="colorpicker_submit"></div></div>',
        defaults = {
      eventName: 'click',
      onShow: function onShow() {},
      onBeforeShow: function onBeforeShow() {},
      onHide: function onHide() {},
      onChange: function onChange() {},
      onSubmit: function onSubmit() {},
      color: 'ff0000',
      livePreview: true,
      flat: false
    },
        fillRGBFields = function fillRGBFields(hsb, cal) {
      var rgb = HSBToRGB(hsb);
      $(cal).data('colorpicker').fields.eq(1).val(rgb.r).end().eq(2).val(rgb.g).end().eq(3).val(rgb.b).end();
    },
        fillHSBFields = function fillHSBFields(hsb, cal) {
      $(cal).data('colorpicker').fields.eq(4).val(hsb.h).end().eq(5).val(hsb.s).end().eq(6).val(hsb.b).end();
    },
        fillHexFields = function fillHexFields(hsb, cal) {
      $(cal).data('colorpicker').fields.eq(0).val(HSBToHex(hsb)).end();
    },
        setSelector = function setSelector(hsb, cal) {
      $(cal).data('colorpicker').selector.css('backgroundColor', '#' + HSBToHex({
        h: hsb.h,
        s: 100,
        b: 100
      }));
      $(cal).data('colorpicker').selectorIndic.css({
        left: parseInt(150 * hsb.s / 100, 10),
        top: parseInt(150 * (100 - hsb.b) / 100, 10)
      });
    },
        setHue = function setHue(hsb, cal) {
      $(cal).data('colorpicker').hue.css('top', parseInt(150 - 150 * hsb.h / 360, 10));
    },
        setCurrentColor = function setCurrentColor(hsb, cal) {
      $(cal).data('colorpicker').currentColor.css('backgroundColor', '#' + HSBToHex(hsb));
    },
        setNewColor = function setNewColor(hsb, cal) {
      $(cal).data('colorpicker').newColor.css('backgroundColor', '#' + HSBToHex(hsb));
    },
        keyDown = function keyDown(ev) {
      var pressedKey = ev.charCode || ev.keyCode || -1;

      if (pressedKey > charMin && pressedKey <= 90 || pressedKey == 32) {
        return false;
      }

      var cal = $(this).parent().parent();

      if (cal.data('colorpicker').livePreview === true) {
        change.apply(this);
      }
    },
        change = function change(ev) {
      var cal = $(this).parent().parent(),
          col;

      if (this.parentNode.className.indexOf('_hex') > 0) {
        cal.data('colorpicker').color = col = HexToHSB(fixHex(this.value));
      } else if (this.parentNode.className.indexOf('_hsb') > 0) {
        cal.data('colorpicker').color = col = fixHSB({
          h: parseInt(cal.data('colorpicker').fields.eq(4).val(), 10),
          s: parseInt(cal.data('colorpicker').fields.eq(5).val(), 10),
          b: parseInt(cal.data('colorpicker').fields.eq(6).val(), 10)
        });
      } else {
        cal.data('colorpicker').color = col = RGBToHSB(fixRGB({
          r: parseInt(cal.data('colorpicker').fields.eq(1).val(), 10),
          g: parseInt(cal.data('colorpicker').fields.eq(2).val(), 10),
          b: parseInt(cal.data('colorpicker').fields.eq(3).val(), 10)
        }));
      }

      if (ev) {
        fillRGBFields(col, cal.get(0));
        fillHexFields(col, cal.get(0));
        fillHSBFields(col, cal.get(0));
      }

      setSelector(col, cal.get(0));
      setHue(col, cal.get(0));
      setNewColor(col, cal.get(0));
      cal.data('colorpicker').onChange.apply(cal, [col, HSBToHex(col), HSBToRGB(col)]);
    },
        blur = function blur(ev) {
      var cal = $(this).parent().parent();
      cal.data('colorpicker').fields.parent().removeClass('colorpicker_focus');
    },
        focus = function focus() {
      charMin = this.parentNode.className.indexOf('_hex') > 0 ? 70 : 65;
      $(this).parent().parent().data('colorpicker').fields.parent().removeClass('colorpicker_focus');
      $(this).parent().addClass('colorpicker_focus');
    },
        downIncrement = function downIncrement(ev) {
      var field = $(this).parent().find('input').focus();
      var current = {
        el: $(this).parent().addClass('colorpicker_slider'),
        max: this.parentNode.className.indexOf('_hsb_h') > 0 ? 360 : this.parentNode.className.indexOf('_hsb') > 0 ? 100 : 255,
        y: ev.pageY,
        field: field,
        val: parseInt(field.val(), 10),
        preview: $(this).parent().parent().data('colorpicker').livePreview
      };
      $(document).bind('mouseup', current, upIncrement);
      $(document).bind('mousemove', current, moveIncrement);
    },
        moveIncrement = function moveIncrement(ev) {
      ev.data.field.val(Math.max(0, Math.min(ev.data.max, parseInt(ev.data.val + ev.pageY - ev.data.y, 10))));

      if (ev.data.preview) {
        change.apply(ev.data.field.get(0), [true]);
      }

      return false;
    },
        upIncrement = function upIncrement(ev) {
      change.apply(ev.data.field.get(0), [true]);
      ev.data.el.removeClass('colorpicker_slider').find('input').focus();
      $(document).unbind('mouseup', upIncrement);
      $(document).unbind('mousemove', moveIncrement);
      return false;
    },
        downHue = function downHue(ev) {
      var current = {
        cal: $(this).parent(),
        y: $(this).offset().top
      };
      current.preview = current.cal.data('colorpicker').livePreview;
      $(document).bind('mouseup', current, upHue);
      $(document).bind('mousemove', current, moveHue);
    },
        moveHue = function moveHue(ev) {
      change.apply(ev.data.cal.data('colorpicker').fields.eq(4).val(parseInt(360 * (150 - Math.max(0, Math.min(150, ev.pageY - ev.data.y))) / 150, 10)).get(0), [ev.data.preview]);
      return false;
    },
        upHue = function upHue(ev) {
      fillRGBFields(ev.data.cal.data('colorpicker').color, ev.data.cal.get(0));
      fillHexFields(ev.data.cal.data('colorpicker').color, ev.data.cal.get(0));
      $(document).unbind('mouseup', upHue);
      $(document).unbind('mousemove', moveHue);
      return false;
    },
        downSelector = function downSelector(ev) {
      var current = {
        cal: $(this).parent(),
        pos: $(this).offset()
      };
      current.preview = current.cal.data('colorpicker').livePreview;
      $(document).bind('mouseup', current, upSelector);
      $(document).bind('mousemove', current, moveSelector);
    },
        moveSelector = function moveSelector(ev) {
      change.apply(ev.data.cal.data('colorpicker').fields.eq(6).val(parseInt(100 * (150 - Math.max(0, Math.min(150, ev.pageY - ev.data.pos.top))) / 150, 10)).end().eq(5).val(parseInt(100 * Math.max(0, Math.min(150, ev.pageX - ev.data.pos.left)) / 150, 10)).get(0), [ev.data.preview]);
      return false;
    },
        upSelector = function upSelector(ev) {
      fillRGBFields(ev.data.cal.data('colorpicker').color, ev.data.cal.get(0));
      fillHexFields(ev.data.cal.data('colorpicker').color, ev.data.cal.get(0));
      $(document).unbind('mouseup', upSelector);
      $(document).unbind('mousemove', moveSelector);
      return false;
    },
        enterSubmit = function enterSubmit(ev) {
      $(this).addClass('colorpicker_focus');
    },
        leaveSubmit = function leaveSubmit(ev) {
      $(this).removeClass('colorpicker_focus');
    },
        clickSubmit = function clickSubmit(ev) {
      var cal = $(this).parent();
      var col = cal.data('colorpicker').color;
      cal.data('colorpicker').origColor = col;
      setCurrentColor(col, cal.get(0));
      cal.data('colorpicker').onSubmit(col, HSBToHex(col), HSBToRGB(col), cal.data('colorpicker').el);
    },
        show = function show(ev) {
      var cal = $('#' + $(this).data('colorpickerId'));
      cal.data('colorpicker').onBeforeShow.apply(this, [cal.get(0)]);
      var pos = $(this).offset();
      var viewPort = getViewport();
      var top = pos.top + this.offsetHeight;
      var left = pos.left;

      if (top + 176 > viewPort.t + viewPort.h) {
        top -= this.offsetHeight + 176;
      }

      if (left + 356 > viewPort.l + viewPort.w) {
        left -= 356;
      }

      cal.css({
        left: left + 'px',
        top: top + 'px'
      });

      if (cal.data('colorpicker').onShow.apply(this, [cal.get(0)]) != false) {
        cal.show();
      }

      $(document).bind('mousedown', {
        cal: cal
      }, hide);
      return false;
    },
        hide = function hide(ev) {
      if (!isChildOf(ev.data.cal.get(0), ev.target, ev.data.cal.get(0))) {
        if (ev.data.cal.data('colorpicker').onHide.apply(this, [ev.data.cal.get(0)]) != false) {
          ev.data.cal.hide();
        }

        $(document).unbind('mousedown', hide);
      }
    },
        isChildOf = function isChildOf(parentEl, el, container) {
      if (parentEl == el) {
        return true;
      }

      if (parentEl.contains) {
        return parentEl.contains(el);
      }

      if (parentEl.compareDocumentPosition) {
        return !!(parentEl.compareDocumentPosition(el) & 16);
      }

      var prEl = el.parentNode;

      while (prEl && prEl != container) {
        if (prEl == parentEl) return true;
        prEl = prEl.parentNode;
      }

      return false;
    },
        getViewport = function getViewport() {
      var m = document.compatMode == 'CSS1Compat';
      return {
        l: window.pageXOffset || (m ? document.documentElement.scrollLeft : document.body.scrollLeft),
        t: window.pageYOffset || (m ? document.documentElement.scrollTop : document.body.scrollTop),
        w: window.innerWidth || (m ? document.documentElement.clientWidth : document.body.clientWidth),
        h: window.innerHeight || (m ? document.documentElement.clientHeight : document.body.clientHeight)
      };
    },
        fixHSB = function fixHSB(hsb) {
      return {
        h: Math.min(360, Math.max(0, hsb.h)),
        s: Math.min(100, Math.max(0, hsb.s)),
        b: Math.min(100, Math.max(0, hsb.b))
      };
    },
        fixRGB = function fixRGB(rgb) {
      return {
        r: Math.min(255, Math.max(0, rgb.r)),
        g: Math.min(255, Math.max(0, rgb.g)),
        b: Math.min(255, Math.max(0, rgb.b))
      };
    },
        fixHex = function fixHex(hex) {
      var len = 6 - hex.length;

      if (len > 0) {
        var o = [];

        for (var i = 0; i < len; i++) {
          o.push('0');
        }

        o.push(hex);
        hex = o.join('');
      }

      return hex;
    },
        HexToRGB = function HexToRGB(hex) {
      var hex = parseInt(hex.indexOf('#') > -1 ? hex.substring(1) : hex, 16);
      return {
        r: hex >> 16,
        g: (hex & 0x00FF00) >> 8,
        b: hex & 0x0000FF
      };
    },
        HexToHSB = function HexToHSB(hex) {
      return RGBToHSB(HexToRGB(hex));
    },
        RGBToHSB = function RGBToHSB(rgb) {
      var hsb = {
        h: 0,
        s: 0,
        b: 0
      };
      var min = Math.min(rgb.r, rgb.g, rgb.b);
      var max = Math.max(rgb.r, rgb.g, rgb.b);
      var delta = max - min;
      hsb.b = max;

      if (max != 0) {}

      hsb.s = max != 0 ? 255 * delta / max : 0;

      if (hsb.s != 0) {
        if (rgb.r == max) {
          hsb.h = (rgb.g - rgb.b) / delta;
        } else if (rgb.g == max) {
          hsb.h = 2 + (rgb.b - rgb.r) / delta;
        } else {
          hsb.h = 4 + (rgb.r - rgb.g) / delta;
        }
      } else {
        hsb.h = -1;
      }

      hsb.h *= 60;

      if (hsb.h < 0) {
        hsb.h += 360;
      }

      hsb.s *= 100 / 255;
      hsb.b *= 100 / 255;
      return hsb;
    },
        HSBToRGB = function HSBToRGB(hsb) {
      var rgb = {};
      var h = Math.round(hsb.h);
      var s = Math.round(hsb.s * 255 / 100);
      var v = Math.round(hsb.b * 255 / 100);

      if (s == 0) {
        rgb.r = rgb.g = rgb.b = v;
      } else {
        var t1 = v;
        var t2 = (255 - s) * v / 255;
        var t3 = (t1 - t2) * (h % 60) / 60;
        if (h == 360) h = 0;

        if (h < 60) {
          rgb.r = t1;
          rgb.b = t2;
          rgb.g = t2 + t3;
        } else if (h < 120) {
          rgb.g = t1;
          rgb.b = t2;
          rgb.r = t1 - t3;
        } else if (h < 180) {
          rgb.g = t1;
          rgb.r = t2;
          rgb.b = t2 + t3;
        } else if (h < 240) {
          rgb.b = t1;
          rgb.r = t2;
          rgb.g = t1 - t3;
        } else if (h < 300) {
          rgb.b = t1;
          rgb.g = t2;
          rgb.r = t2 + t3;
        } else if (h < 360) {
          rgb.r = t1;
          rgb.g = t2;
          rgb.b = t1 - t3;
        } else {
          rgb.r = 0;
          rgb.g = 0;
          rgb.b = 0;
        }
      }

      return {
        r: Math.round(rgb.r),
        g: Math.round(rgb.g),
        b: Math.round(rgb.b)
      };
    },
        RGBToHex = function RGBToHex(rgb) {
      var hex = [rgb.r.toString(16), rgb.g.toString(16), rgb.b.toString(16)];
      $.each(hex, function (nr, val) {
        if (val.length == 1) {
          hex[nr] = '0' + val;
        }
      });
      return hex.join('');
    },
        HSBToHex = function HSBToHex(hsb) {
      return RGBToHex(HSBToRGB(hsb));
    },
        restoreOriginal = function restoreOriginal() {
      var cal = $(this).parent();
      var col = cal.data('colorpicker').origColor;
      cal.data('colorpicker').color = col;
      fillRGBFields(col, cal.get(0));
      fillHexFields(col, cal.get(0));
      fillHSBFields(col, cal.get(0));
      setSelector(col, cal.get(0));
      setHue(col, cal.get(0));
      setNewColor(col, cal.get(0));
    };

    return {
      init: function init(opt) {
        opt = $.extend({}, defaults, opt || {});

        if (typeof opt.color == 'string') {
          opt.color = HexToHSB(opt.color);
        } else if (opt.color.r != undefined && opt.color.g != undefined && opt.color.b != undefined) {
          opt.color = RGBToHSB(opt.color);
        } else if (opt.color.h != undefined && opt.color.s != undefined && opt.color.b != undefined) {
          opt.color = fixHSB(opt.color);
        } else {
          return this;
        }

        return this.each(function () {
          if (!$(this).data('colorpickerId')) {
            var options = $.extend({}, opt);
            options.origColor = opt.color;
            var id = 'collorpicker_' + parseInt(Math.random() * 1000);
            $(this).data('colorpickerId', id);
            var cal = $(tpl).attr('id', id);

            if (options.flat) {
              cal.appendTo(this).show();
            } else {
              cal.appendTo(document.body);
            }

            options.fields = cal.find('input').bind('keyup', keyDown).bind('change', change).bind('blur', blur).bind('focus', focus);
            cal.find('span').bind('mousedown', downIncrement).end().find('>div.colorpicker_current_color').bind('click', restoreOriginal);
            options.selector = cal.find('div.colorpicker_color').bind('mousedown', downSelector);
            options.selectorIndic = options.selector.find('div div');
            options.el = this;
            options.hue = cal.find('div.colorpicker_hue div');
            cal.find('div.colorpicker_hue').bind('mousedown', downHue);
            options.newColor = cal.find('div.colorpicker_new_color');
            options.currentColor = cal.find('div.colorpicker_current_color');
            cal.data('colorpicker', options);
            cal.find('div.colorpicker_submit').bind('mouseenter', enterSubmit).bind('mouseleave', leaveSubmit).bind('click', clickSubmit);
            fillRGBFields(options.color, cal.get(0));
            fillHSBFields(options.color, cal.get(0));
            fillHexFields(options.color, cal.get(0));
            setHue(options.color, cal.get(0));
            setSelector(options.color, cal.get(0));
            setCurrentColor(options.color, cal.get(0));
            setNewColor(options.color, cal.get(0));

            if (options.flat) {
              cal.css({
                position: 'relative',
                display: 'block'
              });
            } else {
              $(this).bind(options.eventName, show);
            }
          }
        });
      },
      showPicker: function showPicker() {
        return this.each(function () {
          if ($(this).data('colorpickerId')) {
            show.apply(this);
          }
        });
      },
      hidePicker: function hidePicker() {
        return this.each(function () {
          if ($(this).data('colorpickerId')) {
            $('#' + $(this).data('colorpickerId')).hide();
          }
        });
      },
      setColor: function setColor(col) {
        if (typeof col == 'string') {
          col = HexToHSB(col);
        } else if (col.r != undefined && col.g != undefined && col.b != undefined) {
          col = RGBToHSB(col);
        } else if (col.h != undefined && col.s != undefined && col.b != undefined) {
          col = fixHSB(col);
        } else {
          return this;
        }

        return this.each(function () {
          if ($(this).data('colorpickerId')) {
            var cal = $('#' + $(this).data('colorpickerId'));
            cal.data('colorpicker').color = col;
            cal.data('colorpicker').origColor = col;
            fillRGBFields(col, cal.get(0));
            fillHSBFields(col, cal.get(0));
            fillHexFields(col, cal.get(0));
            setHue(col, cal.get(0));
            setSelector(col, cal.get(0));
            setCurrentColor(col, cal.get(0));
            setNewColor(col, cal.get(0));
          }
        });
      }
    };
  }();

  $.fn.extend({
    ColorPicker: ColorPicker.init,
    ColorPickerHide: ColorPicker.hidePicker,
    ColorPickerShow: ColorPicker.showPicker,
    ColorPickerSetColor: ColorPicker.setColor
  });
})(jQuery);

/***/ }),

/***/ "./resources/assets/js/components/element.js":
/*!***************************************************!*\
  !*** ./resources/assets/js/components/element.js ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _objectJson_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./objectJson.js */ "./resources/assets/js/components/objectJson.js");


function element() {}

;
var w = window;
element.prototype = new _objectJson_js__WEBPACK_IMPORTED_MODULE_0__["default"]();

element.prototype.dragdrop = function (el, obj) {
  if ($(el.item).hasClass('box')) {
    var file = $(el.item).attr('value');
    $.ajax({
      url: "./frontend_asset/json/childs/" + file,
      dataType: "json",
      success: function success(response) {
        if (el.helper != null) {
          obj.content.splice(el.helper.index(), 0, response);
        }

        response.content.push(element.prototype.control());
        $(el.helper).replaceWith(element.prototype.draw(response));
      },
      error: function error(response) {
        console.log("error : ", response);
      }
    });
  } else if ($(el.item).hasClass('item-user')) {
    var _file = $(el.item).attr('value');

    $.ajax({
      url: "./frontend_asset/json/template/" + _file,
      dataType: "json",
      success: function success(response) {
        var parentTag = $(el.helper).parent();
        var temp = el.helper;
        $.each(response, function (k, v) {
          obj.content.splice(el.helper.index() + k, 0, v);
          var tag = element.prototype.draw(v);
          $(temp).after(tag);
          temp = tag;
        });
        $(el.helper).remove();
        element.prototype.sortableColumn();
        element.prototype.sortableArea();
      },
      error: function error(response) {
        console.log("error : ", response);
      }
    });
  } else {
    var ele = $(el.item).children().first()[0];
    var grid = element.prototype.createJsonGrid(ele);
    grid.content.push(element.prototype.control());

    if (el.helper != null) {
      obj.content.splice(el.helper.index(), 0, grid);
    }

    $(el.helper).replaceWith(element.prototype.draw(grid));
  }
};

element.prototype.createJsonGrid = function (el) {
  var cols = $(el).val().split(" ", 12);
  var obj = {
    "tag": "div",
    "category": "grid",
    "attr": {
      "class": "row",
      "style": {}
    },
    "content": []
  };
  $.each(cols, function (index, value) {
    var col = {
      "tag": "div",
      "category": "grid",
      "attr": {
        "sort": "" + index,
        "class": "col-md-" + value + " column",
        "style": {}
      },
      "content": []
    };
    obj.content.push(col);
  });
  return obj;
};

element.prototype.getParent = function (el) {
  if ($(el).parent().attr("id") == "sortable-area2") {
    return object;
  } else {
    //duyet de tim vi tri
    var arr = [];
    $.each($(el).parents(), function (k, v) {
      arr.push($(v).index());
      if ($(v).hasClass('group')) return false;
    });
    arr.pop();
    arr = arr.reverse();
    var root = window.object;
    $.each(arr, function (j, i) {
      root = root.content[i];
    });
    return root;
  }
};

element.prototype.removeNodeObject = function (el) {
  var objParent = element.prototype.getParent(el);
  var index = $(el).index();
  objParent.content.splice(index, 1);
};

element.prototype.editElement = function (el, obj) {
  $.each($('.edit-box select, .edit-box input'), function (k, v) {
    $(v).on({
      keyup: edit,
      change: edit
    });
  });

  function edit() {
    var name = $(this).attr('name');
    var value = $(this).val();
    if ($(this).attr('type') == 'number') value = value + 'px';

    if ($(this).data('type') == "attr") {
      $(el).attr(name, value);
      $(this).val() == "" ? delete obj.attr[name] : obj.attr[name] = value;
    } else {
      $(el).css(name, value);
      $(this).val() == "" ? delete obj.attr.style[name] : obj.attr.style[name] = value;
    }
  }
};

element.prototype.editBaseStyle = function (el, obj) {
  $('.right .selectable .base-text-item').click(function () {
    if ($(this).parent().hasClass('select-one')) {
      if ($(this).hasClass('selected')) {
        $(this).toggleClass('selected');
      } else {
        $(this).parent().find('.selected').removeClass('selected');
        $(this).addClass('selected');
      }
    } else {
      $(this).toggleClass('selected');
    }

    var name = $(this).data('attr-name');
    var value = $(this).data('attr-value');

    if ($(this).hasClass('selected')) {
      $(w.el).css(name, value);
      w.obj.attr.style[name] = value;
    } else {
      $(w.el).css(name, '');
      delete w.obj.attr.style[name];
    }
  });
  $('.right .assignable .base-text-item').on({
    keyup: edit,
    change: edit
  });

  function edit() {
    var name = $(this).attr('name');
    var value = $(this).val();
    if ($(this).hasClass('px')) value = value + 'px';
    $(w.el).css(name, value);
    $(this).val() == "" ? delete w.obj.attr.style[name] : w.obj.attr.style[name] = value;
  }

  ;
  $.each($('#colorpkr-border, #colorpkr-font, #colorpkr-background'), function (k, v) {
    var style = $(v).data('attr-name');
    colorpkr(v, style);
  });

  function colorpkr(div, styleName) {
    $(div).ColorPicker({
      onShow: function onShow(colpkr) {
        $(colpkr).fadeIn(500);
        return false;
      },
      onHide: function onHide(colpkr) {
        $(colpkr).fadeOut(500);
        return false;
      },
      onChange: function onChange(hsb, hex, rgb) {
        $(div).find('div').css('background', '#' + hex);
        $(w.el).css(styleName, '#' + hex);
        w.obj.attr.style[styleName] = '#' + hex;
      }
    });
  }
};

element.prototype.fillStyleInBox = function () {
  var style = window.getComputedStyle(w.el);
  $.each($('.right .selectable .base-text-item'), function (k, v) {
    var attr = $(v).data('attr-name');
    var value = $(v).data('attr-value');
    var response = style.getPropertyValue(attr);
    var formatRespone = response.substring(0, response.indexOf(' '));

    if (response == value || formatRespone == value) {
      $(v).addClass('selected');
    }
  });
  $.each($('.right .assignable .base-text-item'), function (k, v) {
    var attr = $(v).attr('name');
    $(v).val(style.getPropertyValue(attr).replace('px', ''));
  });
};

element.prototype.sortableColumn = function () {
  $('#sortable-area2 .column').sortable({
    connectWith: '.column, #sortable-area2',
    handle: '.drag',
    scroll: false,

    /*active: function(e, ui) {
    	if($(ui.item).parents().prop("tagName") == "DIV") {
    		console.log('column active', ui);
    		indexStart = ui.item.index();
    		nodeRemove = element.prototype.getParent(ui.helper);
    	}
    },*/
    start: function start(e, ui) {
      if ($(ui.item).parents().prop("tagName") == "DIV") {
        // console.log('column start', ui);
        w.indexStart = ui.item.index();
        w.nodeRemove = element.prototype.getParent(ui.helper);
      }
    },
    stop: function stop(e, ui) {
      w.indexStop = $(ui.item).index();

      if (indexStop != -1 && !$(ui.item).hasClass('box ui-draggable') && !$(ui.item).hasClass('item-user ui-draggable')) {
        // console.log('column stop', ui);
        w.item = nodeRemove.content.splice(indexStart, 1);
        w.nodeReceive = element.prototype.getParent(ui.item);
        w.nodeReceive.content.splice(indexStop, 0, item[0]);
      }
    },
    receive: function receive(e, ui) {
      if ($(ui.item).parent().prop("tagName") == "LI") {
        // console.log('column receive', ui);
        var tree = element.prototype.getParent(ui.helper);
        element.prototype.dragdrop(ui, tree);
      }
    }
  });
};

element.prototype.sortableArea = function () {
  $('body .sidebar-nav .box, body .sidebar-nav .item-user').draggable({
    connectToSortable: "#sortable-area2, .column",
    helper: "clone",
    handle: ".drag",
    scroll: false
  });
  $('body .sidebar-nav .lyrow').draggable({
    connectToSortable: "#sortable-area2, .column",
    helper: "clone",
    handle: ".drag",
    scroll: false,
    stop: function stop(e, ui) {
      element.prototype.sortableColumn();
    }
  });
  $('#sortable-area2').sortable({
    connectWith: '.column',
    handle: ".drag",
    scroll: false,
    start: function start(e, ui) {
      if ($(ui.item).parents().prop("tagName") == "DIV") {
        // console.log('root start', ui);
        w.indexStart = ui.item.index();
        w.nodeRemove = element.prototype.getParent(ui.helper);
      }
    },

    /*
    active: function(e, ui) {
    if($(ui.item).parents().prop("tagName") == "DIV") {
    console.log('root active', ui);
    indexStart = ui.item.index();
    nodeRemove = element.prototype.getParent(ui.helper);
    }
    },*/
    stop: function stop(e, ui) {
      w.indexStop = $(ui.item).index();

      if (indexStop != -1 && !$(ui.item).hasClass('box ui-draggable') && !$(ui.item).hasClass('item-user ui-draggable')) {
        // console.log('root stop', ui);
        w.item = nodeRemove.content.splice(indexStart, 1);
        w.nodeReceive = element.prototype.getParent(ui.item);
        w.nodeReceive.content.splice(indexStop, 0, item[0]);
      }
    },
    receive: function receive(e, ui) {
      if ($(ui.item).parent().prop("tagName") == "LI") {
        // console.log("root receive", ui);
        var tree = element.prototype.getParent(ui.helper);
        element.prototype.dragdrop(ui, tree);
      }
    }
  });
};

/* harmony default export */ __webpack_exports__["default"] = (element);

/***/ }),

/***/ "./resources/assets/js/components/objectJson.js":
/*!******************************************************!*\
  !*** ./resources/assets/js/components/objectJson.js ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
function objectJson() {}

objectJson.prototype.draw = function (json) {
  var tag = document.createElement(json.tag);
  if (json.category != "") $(tag).attr('data-category', json.category);

  if (json.attr != null) {
    $.each(json.attr, function (k, v) {
      if (typeof v != "string") {
        if (k == "style") {
          $.each(v, function (j, i) {
            $(tag).css(j, i);
          });
        }
      } else {
        $(tag).attr(k, v);
      }
    });
  }

  if (typeof json.content != "string") {
    $.each(json.content, function (k, v) {
      tag.appendChild(objectJson.prototype.draw(v));
    });
  } else {
    $(tag).html(json.content);
  }

  return tag;
};

objectJson.prototype.control = function () {
  var control = {
    "tag": "div",
    "category": "control",
    "attr": {
      "class": "content__control",
      "style": {}
    },
    "content": [{
      "tag": "label",
      "attr": {
        "class": "edit-el btn btn-warning",
        "style": {}
      },
      "content": "edit"
    }, {
      "tag": "label",
      "attr": {
        "class": "drag btn btn-primary",
        "style": {}
      },
      "content": "drag"
    }, {
      "tag": "label",
      "attr": {
        "class": "remove btn btn-danger",
        "style": {}
      },
      "content": "remove"
    }]
  };
  return control;
};

/* harmony default export */ __webpack_exports__["default"] = (objectJson);

/***/ }),

/***/ "./resources/assets/js/custom.js":
/*!***************************************!*\
  !*** ./resources/assets/js/custom.js ***!
  \***************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _components_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./components/element */ "./resources/assets/js/components/element.js");

var ele = new _components_element__WEBPACK_IMPORTED_MODULE_0__["default"]();

window.login = function () {
  $(".modal-title").html('Login');
  $("#form").attr('action', 'login');
  $.get("frontend_asset/partital-views/login-form.html", function (data) {
    $("#group").html(data);
  });
};

window.register = function () {
  $(".modal-title").html('Register');
  $("#form").attr('action', 'register');
  $.get("frontend_asset/partital-views/register-form.html", function (data) {
    $("#group").html(data);
  });
};

$(document).ready(function () {
  $('#login').click(function () {
    login();
  });
  $('#register').click(function () {
    register();
  });
});

window.exportZip = function () {
  var html_content = '';
  var bootstrap_css = "";
  var bootstrap_js = "";
  var jquery = "";
  getData();

  function getData() {
    $.ajax({
      url: "./frontend_asset/partital-views/head-html-file.html",
      success: function success(response) {
        html_content += response;
        $("#sortable-area2 .content__control").remove();
        html_content += $("#sortable-area2")[0].innerHTML;
        html_content += "</body></html>";
        $.ajax({
          url: "./css/bootstrap.min.css",
          success: function success(response) {
            bootstrap_css += response;
            $.ajax({
              url: "./js/bootstrap.min.js",
              success: function success(response) {
                bootstrap_js += response;
                $.ajax({
                  url: "./js/jquery-1.12.4.min.js",
                  success: function success(response) {
                    jquery += response;
                    zip(html_content, bootstrap_css, jquery, bootstrap_js);
                  },
                  error: function error(response) {
                    alert('có lỗi xảy ra!');
                  }
                });
              },
              error: function error(response) {
                alert('có lỗi xảy ra!');
              }
            });
          },
          error: function error(response) {
            alert('có lỗi xảy ra!');
          }
        });
      },
      error: function error(response) {
        alert('có lỗi xảy ra!');
      }
    });
  }

  function zip(html_content, bootstrap_css, jquery, bootstrap_js) {
    html_content = html_content.replace(/contenteditable\=\"true\"/gi, '');
    var zip = new JSZip();
    zip.file("css/bootstrap.min.css", bootstrap_css);
    zip.file("js/jquery.min.js", jquery);
    zip.file("js/bootstrap.min.js", bootstrap_js);
    zip.file("index.html", html_content);
    var file_name = Math.floor(Math.random() * 10000) + "_snap-page.zip";
    zip.generateAsync({
      type: "blob"
    }).then(function (blob) {
      saveAs(blob, file_name);
    });
  }
};

window.addCategory = function () {
  $("#modal").modal('show');
  $(".modal-title").html('Add new Category');
  $("#form").attr('action', 'add-category');
  $.get("frontend_asset/partital-views/add-category-form.html", function (htm) {
    $("#group").html(htm);
  });
};

window.showTemplate = function (id) {
  $("#loader").html('<div class="loader" style="margin-top:100px;"></div>');
  $("#control").css('display', '');
  $.ajax({
    url: 'get-template',
    data: {
      'id': id
    },
    dataType: 'json',
    type: 'get',
    success: function success(data) {
      object = JSON.parse(data.content);
      var option = "";
      $("#save").attr('onclick', 'saveEdit()');
      $.ajax({
        url: 'get-category',
        type: 'get',
        success: function success(datas) {
          for (var i = 0; i < datas.length; i++) {
            option += '<option value="' + datas[i].id + '">' + datas[i].name + '</option>';
          }

          $(".modal-title").html('Save layout');
          $("#group").css('height', 'auto');
          $("#form").attr('action', 'edit-template');
          $.get("frontend_asset/partital-views/edit-form.html", function (htm) {
            $("#group").html(htm);
            $("#id_layout").val(data.id);
            $("#name").val(data.name);
          });
        }
      });
      $("#name_template").html("<b id='name-template' style='border-radius:15px;color:#fff; padding:5px; background:orange;'>Editting for:  '" + data.name + "' Template <a href='javascript:void(0);' style='color:red;' onclick='swap()'>X</a></b>");
      $('#loader').hide();
      $(".content #sortable-area2").empty();
      $.each(object.content, function (k, v) {
        $(".content #sortable-area2").append(ele.draw(v));
      }); // register event dreg and sortable for data

      ele.sortableArea();
      ele.sortableColumn();
    }
  });
};

window.swap = function () {
  $("#name-template").hide();
  $("#save").attr('onclick', 'save()');
  $("#sortable-area2").empty();
  $('.right').hide();
  object.content = [];
};

window.saveEdit = function (data) {
  var content = JSON.stringify(object);
  $("#modal").modal('show');
  $(".modal-title").html("Save Template");
  $("#group").append('<input class="decorative-input inspectletIgnore form-control" value=\'' + content + '\' placeholder="" name="content" style="display: none;">');
};

$('body').on('click', '#save_template', function () {
  $(this).parent().append('<input type="text" value="1" name="status">');
}); //save

window.save = function () {
  if ($("body #sortable-area2").children().length > 0) {
    var option = "";
    var content = JSON.stringify(object);
    $("#modal").modal('show');
    $("#group").css('height', '60px');
    $("#group").html('<div class="loader col-md-6 col-md-offset-5"></div>');
    $("#group").css('height', 'auto');
    $.ajax({
      url: 'get-category',
      type: 'get',
      success: function success(data) {
        for (var i = 0; i < data.length; i++) {
          option += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
        }

        $(".modal-title").html('Save layout');
        $("#form").attr('action', 'save-template');
        $.get("frontend_asset/partital-views/add-new.html", function (data) {
          $("#group").html(data);
          $("#content").val(content);
        });
      },
      error: function error(data) {
        $(".modal-title").html('Login');
        $("#form").attr('action', 'login');
        $.get("frontend_asset/partital-views/login-form.html", function (data) {
          $("#group").html(data);
        });
      }
    });
  } else {
    alert("Nothing to save!");
  }
};

$('body').on('click', '#save_template', function () {
  $(this).parent().append('<input type="text" value="1" name="status">');
});
$("#form").validate({
  onfocusout: false,
  onkeyup: false,
  onclick: false,
  rules: {
    "name": {
      required: true
    },
    "email": {
      required: true
    },
    "password": {
      required: true,
      minlength: 6
    },
    "re_password": {
      required: true,
      equalTo: "#password",
      minlength: 6
    },
    "description": {
      required: true
    },
    "category": {
      required: true
    }
  },
  messages: {
    "name": {
      required: "This field is required!"
    },
    "email": {
      require: "This field is required!"
    },
    "password": {
      required: "This field is required!",
      minlength: "Min length 6 digits"
    },
    "re_password": {
      required: "This field is required!",
      equalTo: "Two passwords must be same!",
      minlength: "Min length 6 digits"
    },
    "description": {
      required: "This field is required!"
    },
    "category": {
      required: "Please select an item!"
    }
  }
});

/***/ }),

/***/ "./resources/assets/js/index2.js":
/*!***************************************!*\
  !*** ./resources/assets/js/index2.js ***!
  \***************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _components_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./components/element */ "./resources/assets/js/components/element.js");
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

window.object = {
  "tag": "div",
  "category": "grid",
  "attr": {
    "id": "sortable-area2",
    "class": "row group",
    "style": {}
  },
  "content": []
};

$(document).ready(function () {
  var ele = new _components_element__WEBPACK_IMPORTED_MODULE_0__["default"]();
  var w = window;
  $.ajax({
    url: './frontend_asset/json/process.php',
    data: {
      listdata: "listdata"
    },
    type: "POST",
    success: function success(response) {
      getFileName(JSON.parse(response));
      ele.sortableArea();
    },
    error: function error(response) {
      console.log("error : ", response);
    }
  });

  function getFileName(response) {
    $.each(response, function (k, v) {
      if (v != "." && v != "..") {
        var name = getFileName.prototype.detachName(v);
        var html = "\n\t\t\t\t\t<div class=\"box\" value=\"".concat(name, ".json\">\n\t\t\t\t\t\t<span>").concat(name, "</span><label class=\"btn btn-primary drag\">drag</label>\n\t\t\t\t\t</div>\n\t\t\t\t");
        $('.left .base-css').append(html);
      }
    });
  }

  getFileName.prototype.detachName = function (name) {
    var arr = name.split('.');
    var tagName = arr[0];
    return tagName;
  }; // validate input creat grid layout


  $('.lyrow input').bind('keyup', validateInputGrid);

  function validateInputGrid() {
    var sum = 0;
    var cols = $(this).val().split(" ", 12);
    $.each(cols, function (index, value) {
      sum = sum + parseInt(value);
    });

    if (sum == 12) {
      $(this).parent().find('label').show();
    } else {
      $(this).parent().find('label').hide();
    }
  } // button remove element


  $('body').on('click', '.content .remove', function () {
    var el = $(this).parent().parent();
    ele.removeNodeObject(el);
    $(el).remove();
    $('.edit-box').empty();
    $('.right').hide();
  }); // animation sidebar

  $('.left-item').hide();
  $('.nav-header').click(function () {
    $('.left-item').hide();
    $(this).next().slideToggle();
  }); // edit element

  $('.right').hide();
  $('body').on('click', '.content .edit-el', function () {
    $('.right').show();
    $('.right .selected').removeClass('selected');
    $('.right .assignable .base-text-item').val('');
    var category = $(this).parent().parent().data("category");
    var box = $('.right .edit-box');
    box.empty();
    var el = null;
    var obj = null;

    if (category == "grid") {
      el = $(this).parent().parent()[0];
      obj = ele.getParent($(this).parent());
    } else {
      el = $(this).parent().siblings()[0];
      obj = ele.getParent(el).content[0];
    }

    $('.active').removeClass('active');
    $(el).addClass('active');
    w.el = null;
    w.obj = null;
    w.el = el;
    w.obj = obj;
    ele.fillStyleInBox();
    ele.editBaseStyle();
    $.ajax({
      url: "./frontend_asset/json/categories/" + category + ".json",
      dataType: "json",
      success: function success(response) {
        createBoxStyle(response, el, box);
        ele.editElement(el, obj);
        readImage(el, obj);
      },
      error: function error(response) {
        console.log("error : ", response);
      }
    });

    function readImage(el, obj) {
      $('.right input[type="file"]').change(function () {
        var reader = new FileReader();

        reader.onload = function (e) {
          $(el).attr('src', e.target.result);
        };

        reader.readAsDataURL(this.files[0]);
        var src = $(this).val();
        var arr = src.split("\\");
        src = arr[arr.length - 1];
        obj.attr.src = "./images/" + src;
        pushImage();
      });
    }

    ;

    function pushImage() {
      var myFormData = new FormData();
      myFormData.append("file", $('.right input[type="file"]').prop('files')[0]);
      $.ajax({
        type: 'POST',
        processData: false,
        // important
        contentType: false,
        // important
        data: myFormData,
        url: "./frontend_asset/images/upload.php",
        success: function success(data) {
          console.log(data);
        }
      });
    }

    ;
  }); // fill style category in box design

  var flag = 0;

  function createBoxStyle(json, el, box) {
    flag++;
    var style = window.getComputedStyle(el);
    $.each(json, function (k, v) {
      var label = document.createElement('label');
      var div = document.createElement('div');
      var pxPattern = ['width', 'height'];
      $(label).html(k);
      $(div).append(label);

      if (_typeof(v) == "object") {
        if (k == "style") {
          createBoxStyle(v, el, box);
        } else {
          var select = document.createElement('select');
          flag % 2 != 0 ? $(select).attr('data-type', 'attr') : $(select).attr('data-type', 'css');
          $(select).attr('name', k);
          $.each(v, function (j, i) {
            var option = document.createElement('option');
            $(option).attr('value', i);
            $(option).css(k, i);
            $(option).html(i);
            $(select).append(option);
          });
          $(div).append(select);
          $(box).append(div);
        }
      } else {
        var input = document.createElement('input');
        flag % 2 != 0 ? $(input).attr('data-type', 'attr') : $(input).attr('data-type', 'css');
        $(input).attr('name', k);

        if (pxPattern.indexOf(k) > -1) {
          $(input).attr('type', 'number');
        }

        if (k == "src") {
          $(input).attr('type', 'file');
        }

        $(input).attr('placeholder', style.getPropertyValue(k));
        $(div).append(input);
        $(box).append(div);
      }
    });
  } // privew page


  $('#preview').click(function () {
    $('.left').hide();
    $('.right').hide();
    $('.content').addClass('sourcepreview');
    $('.active').removeClass('active');
  }); // edit page

  $('#edit').click(function () {
    $('.left').show();
    $('.content').removeClass('sourcepreview');
  }); // delete all element created in page

  $('#delete').click(function () {
    if (confirm("Are you sure?")) {
      $('#sortable-area2').empty();
      $('.edit-box').empty();
      object.content = [];
    }
  });
  $('body').on('keyup', '#sortable-area2', function (e) {
    var objParent = ele.getParent(e.target);
    var obj;
    $.each(objParent.content, function (k, v) {
      if (v.tag.toUpperCase() == e.target.tagName) {
        obj = v;
        return false;
      }
    });
    var arr = ['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'p', 'a', 'small', 'span', 'strong', 'b', 'em', 'i'];
    if (arr.indexOf(obj.tag) > -1) obj.content = e.target.innerText;
  });
});

/***/ }),

/***/ "./resources/assets/sass/app.scss":
/*!****************************************!*\
  !*** ./resources/assets/sass/app.scss ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 0:
/*!***************************************************************************!*\
  !*** multi ./resources/assets/js/app.js ./resources/assets/sass/app.scss ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! C:\xampp\htdocs\snap_page_v3\resources\assets\js\app.js */"./resources/assets/js/app.js");
module.exports = __webpack_require__(/*! C:\xampp\htdocs\snap_page_v3\resources\assets\sass\app.scss */"./resources/assets/sass/app.scss");


/***/ })

/******/ });